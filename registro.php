<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    
    <link href="style.css" rel="stylesheet">
    <script src="validador.js"></script>
    <title>Registro de usuarios</title>
  </head>
  <body >
      <div class="container-fluid">
      <div class="contenedor sombra" >
       <div class="row " >
            <div class="col-3 register-bg">
            
            </div>
            <div class="col-9" >
                     <br>
                     <h1>Registro de usuarios</h1>
                     <hr width="27%" />
 
                     <p>Este formulario permite registrar ususarios en el sistema</p>
                     <hr width="60%" />
                     <br>
               <form action="meperdonas.php" onsubmit="return validar();">
                 <div class="row pading">
                    <div class="col-5 container">
                         <label >Nombre</label>
                         <input type="text" id="nombre" class="form-control" placeholder="Ingrese su nombre" requerid>
                    </div>
                    <div class="col-5 container">
                         <label >Apellidos</label>
                         <input type="text" id="apellido" class="form-control" placeholder="Ingrese apellidos" requerid>
                    </div>
                    <br>
                 </div>
                 <div class="row">
                    <div class="col-5 container">
                         <br>
                         <label >RUT </label>
                         <input type="text" id="rut" class="form-control" placeholder="Ingrese su RUT" requerid>
                    </div>
                    <div class="col-5 container">
                         <br>
                         <label>Comuna</label>
                         <select class="from-control" id="comuna" name="ad" onchange="salta(this.form)">
                         <option selected>Ingrese su comuna
                             <option value="">Hualpén
                             <option value="">Concepción 
                             <option value="">Chiguayante 
                             <option value="">Coronel
                             <option value="">San Pedro 
                             <option value="">Tomé  
                             <option value="">Lota 
                             <option value="">Talcahuano
                             <option value="">Penco
                             <option value="">Hualqui
                             
                 </select>
 
                    </div>
                    
                 </div>
                 <div class="row">
                    <div class="col-5 container">
                    <br>
                         <label >Dirección </label>
                         <input type="text" id="direccion"  class="form-control" placeholder="Ingrese su domicilio" requerid>
                    </div>
                    <div class="col-5 container">
                    <br>
                         <label >Contraseña </label>
                         <input type="password" id="pass" class="form-control" placeholder="Ingrese su contraseña" requerid>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-5 container">
                    <br>
                         <label >Correo electrónico </label>
                         <input type="text" id="correo" class="form-control" placeholder="Ingrese su correo electrónico" requerid>
                    </div>
                    <div class="col-5 container">
                    <br>
                         <label >Teléfono </label>
                         <input type="text" id="telefono" class="form-control" placeholder="Ingrese su teléfono" requerid>
                    </div>
                    <br>
                 </div>
                 <div class="col-12">
                 <br>
                    <div class="container">
                          <button class="btn btn-secondary btn-lg btn-block container" >Registrar</button>
                    </div>
                 
                 </div>
 
             </form>
                     
                        
                 
            </div>
       </div>
       </div>
      </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>