function validar() {
    var rut, correo, usuario, apellidom, apellidop, clave, telefono;
    
    rut = document.getElementById("Rut").value;
    correo = document.getElementById("E-mail").value;
    usuario = document.getElementById("usuarioNombre").value;
    apellidop = document.getElementById("apellido_p").value;
    apellidom = document.getElementById("apellido_m").value;
    clave = document.getElementById("password").value;
    telefono = document.getElementById("telefono").value;

    expresion = /^[1-9]{1}[0-9]{6,7}[-]{1}[0-9kK]{1}/;
    expresioncorreo = /\w+@\w+\.+[a-z]/;
    expresiontexto = /^[a-zA-Z]+$/;
    
    if (rut === "" || correo === "" || usuario === "" || apellidom === "" || apellidop === "" || clave === "" || telefono === "") {
        alert ("los campos estan incompletos");
        return false;        
    }
    else if (rut.length>10) {
        alert ("el rut es muy largo");
        return false;
    }
    else if (!expresion.test(rut)) {
        alert ("el rut no es valido");
        return false;
    }
    else if (!expresioncorreo.test(correo)){
        alert ("el correo no es valido");
        return false;
    }
     else if (usuario.length>60) {
        alert ("el nombre de usuario es muy largo");
        return false;
    } 
    else if (!expresiontexto.test(usuario)) {
        alert ("nombre con caracteres invalidos");
        return false;
    }
    else if (apellidop.length>30) {
        alert ("el primero apellido es muy largo");
        return false;
    }
    else if (!expresiontexto.test(apellidop)) {
        alert ("apellido paterno invalido");
        return false;
    } 
    else if (apellidom.length>30) {
        alert ("el segundo apellido es muy largo");
        return false;
    }
    else if (!expresiontexto.test(apellidom)) {
        alert ("apellido materno es invalido");
        return false;
    }
    else if (clave.length>70) {
        alert ("la clave es muy larga");
        return false;
    }
    else if (telefono.length>10 || telefono.length<7) {
        alert ("el numero es telefonico es invalido");
        return false;
    }
    else if (isNaN(telefono)) {
        alert ("el telefono ingresado no es valido");
        return false;
    }
}