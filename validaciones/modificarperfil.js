function validar() {
    var email, usuario, apellidop, apellidom, telefono, contraseña, contraseña2;
    
    email = document.getElementById("Email").value;
    usuario = document.getElementById("EsuarioNombre").value;
    apellidop = document.getElementById("Apellido_p").value;
    apellidom = document.getElementById("Apellido_m").value;
    telefono = document.getElementById("Telefono").value;
    contraseña = document.getElementById("pass").value;
    contraseña2 = document.getElementById("passnuevo").value;

    expresioncorreo = /\w+@\w+\.+[a-z]/;
    expresiontexto = /^[a-zA-Z]+$/;

    if (email === "" || usuario === "" || apellidop === "" || apellidom === "" || telefono === "" || contraseña === "" || contraseña2 ==="") {
        alert ("los campos estan incompletos");
        return false;        
    }
    else if (!expresioncorreo.test(email)){
        alert ("el correo no es valido");
        return false;
    }
    else if (usuario.length>60) {
        alert ("el nombre de usuario es muy largo");
        return false;
    } 
    else if (!expresiontexto.test(usuario)) {
        alert ("nombre con caracteres invalidos");
        return false;
    }
    else if (apellidop.length>30) {
        alert ("el primero apellido es muy largo");
        return false;
    }
    else if (!expresiontexto.test(apellidop)) {
        alert ("apellido paterno invalido");
        return false;
    } 
    else if (apellidom.length>30) {
        alert ("el segundo apellido es muy largo");
        return false;
    }
    else if (!expresiontexto.test(apellidom)) {
        alert ("apellido materno es invalido");
        return false;
    }
    else if (telefono.length>10 || telefono.length<7) {
        alert ("el numero es telefonico es invalido");
        return false;
    }
    else if (isNaN(telefono)) {
        alert ("el telefono ingresado no es valido");
        return false;
    }
    else if (contraseña.length>70) {
        alert ("la contraseña es muy larga");
        return false;
    }
    else if (contraseña2.length>70) {
        alert ("la contraseña de confirmacion es muy larga");
        return false;
    }
    else if (contraseña != contraseña2) {
        alert ("contraseña no coincide");
        return false;
    }
}