function validar () {
    var email, contraseña;
    email = document.getElementById("correo").value;
    contraseña = document.getElementById("clave").value;

    expresion = /\w+@\w+\.+[a-z]/;

    if (email === "" || contraseña  === "") {
        alert("los campos estan incompletos");
        return false;
    }
    else if (email.length>30) {
        alert("el correo es muy largo");
        return false; 
    }else if (!expresion.test(email)){
        alert("el correo no es valido");
        return false;
    }
}