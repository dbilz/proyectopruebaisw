<!DOCTYPE html>
<html lang="es">
<head>
	<meta  charset="UTF-8">
	<title>login</title>
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet"> 
	<link rel="stylesheet"  href="asset/css/estilo.css">
	<script src="validaciones/loginvalidar.js"></script>
</head>
<body>
	<h1>inicia sesion</h1>
	<form action = "consulta_login.php" method="post" onsubmit="return validar();">
		<input type="text" name="correo" placeholder="correo" id="correo"required>
		<input type="password" name="clave" placeholder="contraseña" id="clave"required>
		<input type="submit" value="Ingresar">
	</form>
</body>
</html>