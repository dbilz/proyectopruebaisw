<!DOCTYPE html>

<html>
<head>
	<title>Modificar usuario </title>

	<meta charset="utf-8">



	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet"  href="asset/css/estilo.css">


	<script src="validaciones/modificarperfil.js"></script>

</head>
<body>
<?php //require 'partials/header.php' ?>

	<h1>Modificar usuario</h1>
	<div class="row">
		<div class=" container col-5">
			<div class="container col-12">
	<form action="modificar.php" method="POST" onsubmit="return validar();"> 
		<h1>Datos modificables</h1>


	
<input type="text" name="E-mail" class="form-control" placeholder="Correo electronico" id="Email" required>
<br>

<input type="text" name="usuarioNombre" class="form-control" placeholder="Nombres" id="EsuarioNombre" required>

<br>

<input type="text" name="apellido_p" class="form-control" placeholder="Apellido paterno" id="Apellido_p" required>

<br>
<input type="text" name="apellido_m" class="form-control" placeholder="Apellido materno" id="Apellido_m" required>
<br>
<input type="text" name="telefono" class="form-control" placeholder="Telefono/Celular" maxlength ="9"id="Telefono" required>
<br>
<input type="password" name="pass"class="form-control" placeholder="password" id="pass" required>
<br>
<input type="password" name="passnuevo" class="form-control" placeholder="password_confirmation" id="passnuevo"required>
<br>
<input type="submit"  class="btn btn-primary" value="Guardar cambios ">
<button type="button" class="btn btn-primary" onClick="history.go(-1);">volver atras</button>

	</div>
	</div>

	</div>


	</form>
</body>
</html>