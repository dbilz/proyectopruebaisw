<!DOCTYPE html>
<html>
<head>
	<title>Modificar aviso </title>
	<link href="https://fonts.gogoleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet"> 
	<link rel="stylesheet"  href="asset/css/estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body>

	<h1>Modificar aviso</h1>
    	
    <div class="form-group">
	<form action="modificar_aviso.php" method="post" enctype="multipart/form-data"> 
        
        <?php
              require "conexion.php";
              session_start();

              $ID_publicacion = $_GET['id_pub'];
              $consulta = "SELECT * FROM publicacion WHERE id_pub = $ID_publicacion";
              $resultado = mysqli_query($conexion,$consulta); 
              $row = $resultado -> fetch_array(MYSQLI_ASSOC);
              
              $consulta2 = "SELECT nombre_img_pub, id_pub FROM imagenes INNER JOIN publicacion ON imagenes.id_img = publicacion.id_img  WHERE id_pub = $ID_publicacion";  
              $resultado2 = mysqli_query($conexion,$consulta2);
              while($fila = mysqli_fetch_array($resultado2))
              {
                  $ruta_img = $fila["nombre_img_pub"];
              }                               
           ?>
           <div class="row">
               <div class=" col-5 container">
                    <div class=" col-12 container">
                    <div class="form-group">
            <label for="tituloAviso">Nuevo título del aviso</label>
            <input type="text" class="form-control" name="tituloAviso" id="tituloAviso" value="<?php echo $row['titulo_pub']?>"> <!--RECOGER DATOS -->
            <input type="HIDDEN" class="form-control" name="idAviso" id="idAviso" value="<?php echo $ID_publicacion?>">
        </div>
        
        <label for="descripcion">Descripción del aviso</label>
        <textarea class="form-control" id="descripcion" name="descripcion" placeholder=""><?php echo $row['descripcion_pub']?></textarea> <!--RECOGER DATOS -->
        <label>Seleccione nueva categoría</label>
        <select name="categorias"	class="form-control">
		<?php
			require "conexion.php";
			$getcat 		= "SELECT * FROM categoria_publicacion ORDER BY nombre_cat";
			$consultacat 	= $conexion -> query($getcat);
			while($row = $consultacat -> fetch_array(MYSQLI_ASSOC))
			{
				$cat_ID = $row['id_cat'];
				$catNombre = $row['nombre_cat'];
				$catNombre = utf8_encode($catNombre);
				?>
					<option value="<?php echo $cat_ID; ?>"><?php echo $catNombre?> </option>
					
				<?php
			}
		?>
        </select>
               
            <label for="perfil">Modificar imagen:</label></td>
            <img class="img col-6" width="400" height="285" src="imagenes/<?php echo $ruta_img;?>">
            <input type="file" name="imagen" size="10" id="imagen"> 
            <br>            
              
                    <input type="submit"  class="btn btn-primary " value="Enviar">  
                
                    <button type="button" class="btn btn-primary "onClick="history.go(-1);">volver atras</button>

                 
    </div> 
	</form>
        
                    </div>
               </div>

           </div>
        
	
</body>
</html>