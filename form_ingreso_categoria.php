<!DOCTYPE html>
<html>
<head>
	<title> Ingreso de categoria </title>

	<meta charset="utf-8">



	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet"  href="asset/css/estilo.css">


	<script src="validaciones/validarcategoria.js"></script>
</head>
<body>
<?php //require 'partials/header.php' ?>

	<h4>Ingreso de Categorías</h4>
	<form action="ingreso_categoria.php" method="POST"onsubmit="return validar();">	
		<input type="text" placeholder="Nombre de categoría" name="categoria" id="categoria">		
		<input type="submit" value="Ingresar Categoría">		
	</form>
	<form action="ingreso_subcategoria.php" method="POST"onsubmit="return validar();">
		<input type="text" placeholder="Nombre de sub categoría" name="sub_categoria" id="sub_categoria">		
		<select name="categoria_padre" class="form-control">
		<option selected>Ingrese Categoría Padre</option>
		<?php
			require "conexion.php";					
			$getCategorias 		= "SELECT * FROM categoria_publicacion ORDER BY id_cat";
			$consultaCategorias 	= $conexion -> query($getCategorias);
			while($row = $consultaCategorias -> fetch_array(MYSQLI_ASSOC))
			{
				$CategoriaID = $row['id_cat'];
				$CategoriaNombre = $row['nombre_cat'];
				$CategoriaNombre = utf8_encode($CategoriaNombre);
				?>
					<option value="<?php echo $CategoriaID; ?>"><?php echo $CategoriaNombre?> </option>
				<?php
			}
		?>
		</select>
	<input type="submit" value="Ingresar Sub Categoría">
	</form>
	
</body>
</html>