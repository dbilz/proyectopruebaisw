<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Lista de avisos</title>
	<link href="https://fonts.gogoleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet"> 
	<link rel="stylesheet"  href="asset/css/estilo.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body>
	<?php //require 'partials/header.php' ?> 
	<h1>Mis avisos activos</h1>

          <?php
              require "conexion.php";
              session_start();
              $rut = $_SESSION['idUser'];            
              $getArticulo	= "SELECT * FROM publicacion WHERE rut = '$rut' AND id_e != 9 AND id_t = 2";
              $consultaArticulos = $conexion -> query($getArticulo);              
          ?>
        <div class="col-9 container">

        <table  class="table" >
     <thead >
        <tr>
            <td>Ver publicación</td>
            <td>Título </td>
            <td>Descripción del aviso</td>
            <td>Modificar aviso</td>
            <td>Ocultar aviso</td>
            <td>Publicar aviso</td>
            <td>Eliminar aviso</td>

        </tr>
        </thead>
      <tbody>
          <?php 
              if($consultaArticulos->num_rows>0){
              while($row = $consultaArticulos -> fetch_array(MYSQLI_ASSOC)){
          ?>
      <tr>
      <td><a class="btn btn-primary" href ="mostrar_publicacion.php?id_pub=<?php echo $row['id_pub']?>">Mostrar</a></td>
      <td> <?php echo $row['titulo_pub']?></td>
      <td> <?php echo $row['descripcion_pub']?></td>
      <td><a class="btn btn-primary" href ="formulario_modificar_aviso.php?id_pub=<?php echo $row['id_pub']?>">Modificar</a></td>
      <td><a class="btn btn-primary" href ="deshabilitaravis.php?id_pub=<?php echo $row['id_pub']?>">Ocultar</a></td>
      <td><a class="btn btn-primary" href ="publicaravis.php?id_pub=<?php echo $row['id_pub']?>">Publicar</a></td>
      <td><a class="btn btn-danger" href ="eliminaravi.php?id_pub=<?php echo $row['id_pub']?>">Eliminar</a></td>

    </tr>

     <?php }} ?>
      
      </tbody> 
    </table>

    <div class="container">
        <form>
        <br>
        <button type="button" style="align-self: right;" class="btn btn-primary " onClick="history.go(-1);">volver atras</button>
        </form>
	 
        </div>
        </div>
        
    
    
          
            
    	
</body>
</html>