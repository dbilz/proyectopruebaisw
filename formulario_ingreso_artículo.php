<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ingresar artículo</title>
	<link href="https://fonts.gogoleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet"> 
	<link rel="stylesheet"  href="asset/css/estilo.css">
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    
    
    
    
    
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body>
	<?php require 'partials/header.php' ?> 
    <div class="col-5 container">
    <h1 style="font-size: 25px;">Ingresar artículo</h1>
    <div class="form-group">
	<form action="ingresar_articulo.php" method="post" enctype="multipart/form-data"> 
        <div class="form-group">
            <label for="tituloAviso">Título del artículo</label>
            <input type="text" class="form-control" name="tituloAviso" placeholder="Ingrese el título del aviso" id="tituloAviso" required>
        </div>  
        <br>    
        <h1 for="Descripción"style="font-size:20px">Descripción del artículo</h1>
        <textarea class="form-control" name="descripcion" id="descripcion" rows="3" maxlength="5000" required></textarea> 
        <br>        
        <h1 for="Descripción"style="font-size:20px">Seleccione categoría</h1>
        <select name="categorias"	class="form-control">
		<?php
			require "conexion.php";
			$getcat 		= "SELECT * FROM categoria_publicacion ORDER BY nombre_cat";
			$consultacat 	= $conexion -> query($getcat);
			while($row = $consultacat -> fetch_array(MYSQLI_ASSOC))
			{
				$cat_ID = $row['id_cat'];
				$catNombre = $row['nombre_cat'];
				$catNombre = utf8_encode($catNombre);
				?>
					<option value="<?php echo $cat_ID; ?>"><?php echo $catNombre?> </option>
					
				<?php
			}
		?>
        </select>
        <br>
            <h1 for="perfil"style="font-size:20px">Imagen:</h1></td>
            <input type="file" name="imagen" size="3000000" id="imagen" required> 
        <input type="submit"  class="btn btn-primary container" value="Enviar">  
    </div> 
    </form>
    <form>
    <button type="button" class="btn btn-primary container" onClick="history.go(-1);">volver atras</button>

    </div>
    
  </form>
</body>
</html>