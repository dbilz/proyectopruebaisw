<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>eliminar categoria </title>



	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet"  href="asset/css/estilo.css">
</head>
<body>
	<?php // require 'partials/header.php' ?> 


	<h1>Eliminar Categoria</h1>
	<form action="deletecat.php" method="post"> 

		<select name="categorias"	class="form-control col-5">
		<?php
			require "conexion.php";
			$getcat 		= "SELECT * FROM categoria_publicacion ORDER BY nombre_cat";
			$consultacat 	= $conexion -> query($getcat);
			while($row = $consultacat -> fetch_array(MYSQLI_ASSOC))
			{
				$cat_ID = $row['id_cat'];
				$catNombre = $row['nombre_cat'];
				$catNombre = utf8_encode($catNombre);
				?>
					<option value="<?php echo $cat_ID; ?>"><?php echo $catNombre?> </option>
					
				<?php
			}
		?>
		</select>
		
		<input type="submit" class="btn btn-primary" value="eliminar categoria ">
		
	</form>
</body>
</html>
