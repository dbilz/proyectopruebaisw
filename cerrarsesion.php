<?php

session_start();

    unset($_SESSION['active']);
    unset($_SESSION['idUser']);
    unset($_SESSION['rol']);
    unset($_SESSION['comuna']); 
    unset($_SESSION['id_estado']); 
    unset($_SESSION['nombre']); 
    unset($_SESSION['apellido_paterno']); 
    unset($_SESSION['apellido_materno']); 
    unset($_SESSION['telefono']); 
    unset($_SESSION['promedio_evaluacion']);
    unset($_SESSION['strikes']); 
    unset($_SESSION['fecha_suspension']);
    unset($_SESSION['correo']);

session_destroy();

header("location: login.php");
exit;
?>