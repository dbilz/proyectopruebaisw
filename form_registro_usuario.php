<!DOCTYPE html>
<html>
<head>

	<title>Registrar usuario </title>

	<meta charset="utf-8">




	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet"  href="asset/css/estilo.css">

	<script src="validaciones/registrovalidar.js"></script>
</head>
<body>
	<?php require 'partials/header.php' ?> 
	<div class="row ">
		<div class="container">
		<div class="container container0 col-5">
		
		<h1 aling="center">Ingresar datos </h1>
		<form action="ingresar_usuario.php"  method="post"> 
			<br>
			<div class="row">
				<div class="col-6 container ">
				<input type="text" name="Rut" class="form-control" placeholder="RUT:12345678-9" maxlength="10" id="Rut"required>
				</div>
				<div class="col-6 container">
				<input type="text" name="usuarioNombre"class="form-control"  placeholder="Nombres" id="usuarioNombre"required >

				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-6 container">
				<input type="text" name="apellido_p" class="form-control" placeholder="Apellido paterno" id="apellido_p"required>					</div>
				<div class="col-6 container">
				<input type="text" name="apellido_m" class="form-control"  placeholder="Apellido materno" id="apellido_m"required>		

				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-6 container">
				<input type="text" name="E-mail"  class="form-control" placeholder="Correo electronico" id="E-mail" required>
				</div>
				<div class="col-6 container">
				<input type="text" name="telefono" class="form-control" placeholder="Telefono/Celular" id="telefono"required>

				</div>
			</div>
			<br>
			
			<div class="row">
				<div class="col-6 container">

				<select name="regiones"	class="form-control">
				<?php
				require "conexion.php";
				$getRegiones 		= "SELECT * FROM regiones ORDER BY region_id";
				$consultaRegiones 	= $conexion -> query($getRegiones);
				while($row = $consultaRegiones -> fetch_array(MYSQLI_ASSOC))
				{	
					$RegionID = $row['region_id'];
					$RegionNombre = $row['region_nombre'];
					$RegionNombre = utf8_encode($RegionNombre);
					?>
						<option value="<?php echo $RegionID; ?>"><?php echo $RegionNombre?> </option>
						<?php
				}
					?>
				</select>


				</div>
				<div class="col-6 container">
					<select name="comunas" class="form-control">
					<?php			
					$getComunas 		= "SELECT * FROM comunas ORDER BY comuna_nombre";
					$consultaComunas 	= $conexion -> query($getComunas);
					while($rowComunas = $consultaComunas -> fetch_array(MYSQLI_ASSOC))
					{
					$ComunaID = $rowComunas['comuna_id'];
					$ComunaNombre = $rowComunas['comuna_nombre'];
					$ComunaNombre = utf8_encode($ComunaNombre);
					?>
					<option value="<?php echo $ComunaID; ?>"><?php echo $ComunaNombre?> </option>
					<?php
			}
					?>
					</select>
				</div>
			</div>

			<br>

			
			<div class="row">
			<div class=" col-12">
				<input type="password" class="form-control" name="password" placeholder="Contraseña" id="password"required>

			</div>
			
			</div>
			<br>
			<div class="raw">
				<div >
				<input type="submit" class=" width-35 pull-center btn btn-primary form-control" value="Registrar">
				</div>


			</div>				
		</form>
		
	
	</div>
		</div>	
		
	</div>




	
	
</body>
</html>